(function() {

	var ROLES = {
		ADMIN: 0,
		PERFIL01: 1,
		PERFIL02: 2,
		PERFIL03: 3,
		MESTRE: 999,
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i=0; i<ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') c = c.substring(1);
			if (c.indexOf(name) == 0) return decodeURIComponent(c.substring(name.length,c.length));
		}
		return "";
	}

	function getBreadCrumbsPath() {

		var translateMenu = {
			'aplica': 'Aplicação',
			'estrategia': 'Estratégia',
			'liguagem': 'Linguagem',
		}

		var translateSection = {
			'bras': 'Brastemp',
			'des': 'Design',
			'gour': 'Gourmand',
		}

		var regex = /(\w+)-(\w+)-?(\w+){0,1}/g;
		var matches = regex.exec(window.location.pathname);

		if(matches != null) {
			var menu = matches[1];
			var section = translateSection.hasOwnProperty(matches[2]) ? translateSection[matches[2]] : matches[2];
			var subsection = typeof matches[3] != 'undefined' ? (' > ' + matches[3]) : '';
			var currentSlide = $('section.selected').attr('name');
			currentSlide = typeof currentSlide != 'undefined' ? ' > ' + currentSlide.replace(/_/g, ' ') : '';

			$('#breadcrumbs').html(translateMenu[menu] + ' > ' + section + subsection + currentSlide);
		}
	}

	function setBreadcrumbs() {

		$('.slides').bind('mousewheel resize', function(event) {
			getBreadCrumbsPath();
		});

		$('.navigation li').on("click touchend", function(){
      		getBreadCrumbsPath();
    	});

    	$('.nextSlide').on('mouseup touchstart', function(){
    		getBreadCrumbsPath();
  		});

    	$(window).on("keydown",function(e){
    		if (e.keyCode === 37 
    			|| e.keyCode === 38
    			|| e.keyCode === 39
    			|| e.keyCode === 40) {
    			
    			getBreadCrumbsPath();
    		}
    	});

    	getBreadCrumbsPath();
	}

	function autoComplete() {
		$("#buscar").autocomplete({
			source: function(req, res) {
				$.ajax({
					url: "/buscar/" + req.term,
					success: function(data) {
						res(data);
					}
				});
			},
			minLength: 3,
			select: function(event, ui) {
				window.location.href = "/" + ui.item.url;
			}
		})
		.autocomplete( "instance" )._renderItem = function( ul, item ) {
      		return $( "<li>" )
        		.append( '<span class="title">' + item.title + '</span><br/>' + item.description )
        		.appendTo( ul );
    	};
	}

	function setProfile() {
		var fUserName = document.getElementById('user_name');
		fUserName.innerHTML = getCookie('brandbook_brastemp_user_name').toLowerCase();

		var cUserRole = getCookie('brandbook_brastemp_user_role');

		var linkAdmin = document.getElementById('link_admin');
		var menuEstrategia = document.getElementById('menu_estrategia');
		var menuLinguagem = document.getElementById('menu_linguagem');
		var menuAplicacao = document.getElementById('menu_aplicacao');
		var isAdmin = (cUserRole == ROLES.ADMIN || cUserRole == ROLES.MESTRE);

		if(isAdmin && linkAdmin != null) {
			linkAdmin.style.display = 'inline';
		}

		if(cUserRole == ROLES.PERFIL03 || isAdmin) {
			menuEstrategia.style.display = 'block';
			menuLinguagem.style.display = 'block';
			menuAplicacao.style.display = 'block';
		}

		if(cUserRole == ROLES.PERFIL01) {
			menuAplicacao.style.display = 'block';
		}

		if(cUserRole == ROLES.PERFIL02) {
			menuAplicacao.style.display = 'block';
			menuLinguagem.style.display = 'block';
		}
	}

	$(document).ready(function() {
		autoComplete();
		setProfile();
		setBreadcrumbs();
	});

})();


jQuery(document).ready(function($){
    
    $('#busca-btn').on('click', function(e){
        e.preventDefault();
        buscar();
    });
    
    var sflag = false;
    
    function buscar() {
        
        // mobile
        if( $('#buscar').css('position') == 'absolute' ){
            
            // abrir
            if( sflag == false ){
                //console.log('abrir');
                $('#buscar').addClass('opn');
                $('.panel.top .sections .right .square').addClass('opn');
                $('#buscar').focus();
                sflag = true;
            }
            // buscar
            else{
                //console.log('buscar');
                $('#buscar').autocomplete('search', $('#buscar').val());
            }
        }
        // desktop
        else{
            //console.log('busca normal');
            $('#buscar').autocomplete('search', $('#buscar').val());
        }
    }

    /**
     * Fechar buscar ao clicar fora
     * 
     */
    $('html').click(function() {
        //console.log('fechar');
        $('#buscar').removeClass('opn');
        $('.panel.top .sections .right .square').removeClass('opn');
        sflag = false;
    });
    
    $('#buscar, .panel.top .sections .right .square').click(function(event){
        event.stopPropagation();
    });
    
    /**
     * IMAGE MAP RESPONSIVO
     * 
     */
    $('img[usemap]').rwdImageMaps();
});


/*
* rwdImageMaps jQuery plugin v1.5
*
* Allows image maps to be used in a responsive design by recalculating the area coordinates to match the actual image size on load and window.resize
*
* Copyright (c) 2013 Matt Stow
* https://github.com/stowball/jQuery-rwdImageMaps
* http://mattstow.com
* Licensed under the MIT license
*/
;(function(a){a.fn.rwdImageMaps=function(){var c=this;var b=function(){c.each(function(){if(typeof(a(this).attr("usemap"))=="undefined"){return}var e=this,d=a(e);a("<img />").load(function(){var g="width",m="height",n=d.attr(g),j=d.attr(m);if(!n||!j){var o=new Image();o.src=d.attr("src");if(!n){n=o.width}if(!j){j=o.height}}var f=d.width()/100,k=d.height()/100,i=d.attr("usemap").replace("#",""),l="coords";a('map[name="'+i+'"]').find("area").each(function(){var r=a(this);if(!r.data(l)){r.data(l,r.attr(l))}var q=r.data(l).split(","),p=new Array(q.length);for(var h=0;h<p.length;++h){if(h%2===0){p[h]=parseInt(((q[h]/n)*100)*f)}else{p[h]=parseInt(((q[h]/j)*100)*k)}}r.attr(l,p.toString())})}).attr("src",d.attr("src"))})};a(window).resize(b).trigger("resize");return this}})(jQuery);
